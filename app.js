function app() {
    pageLoad()
    scrollPage()
    handleGoToTop()
}
app()

// bind querySelector vs querySelectorAll
const $= document.querySelector.bind(document)
const $$= document.querySelectorAll.bind(document)

// element parent .body__mainCar--info 
const bodyMainCarInfos= $$('.body__mainCar--info')
// element childrens .body__mainCar--info
const infoTitles= $$('.info--title')
const infoHeadings= $$('.info--heading')
const infoDetails= $$('.body__mainCar--info .info--detail')
const infoBtns= $$('.info--btn')
const lines= $$('.body__mainCar--info .line')
const dataAnimationSlide= [infoTitles,infoHeadings,infoDetails,infoBtns,lines,]

// element parent .body__introduce 
const bodyIntroduce= $$('.body__introduce')
// element childrens .body__introduce
const img= $$('.body__introduce--img img')
const introduceInfoHeading= $$('.introduce__info--heading')
const introduceInforDetail= $$('.introduce__info--detail')
const dataAnimationScale= [img,introduceInfoHeading,introduceInforDetail]

// element childrens
const sloganP= $('.body__slogan p')
const sloganH= $('.body__slogan h1')
const sloganLine= $('.body__slogan .line')
const sloganBtn= $('.body__slogan .btn.action')
const dataPageLoadSlide= [sloganP,sloganH,sloganLine,sloganBtn]

// element childrens
const headerLogo= $('.header__logo img')
const headerTitleProvider= $('.header__title--provider')
const headerTitleOptional= $('.header__title--optional')
const dataPageLoadAppear= [headerLogo,headerTitleProvider,headerTitleOptional]


// animation when scrollPage
function scrollPage() {
    window.addEventListener('scroll', () => {
        handleScrollPage(dataAnimationSlide)
        handleScrollPage(dataAnimationScale)
    })
}

function handleScrollPage(bigDatas) {
    const heightWindow= window.innerHeight
    bigDatas.forEach((elements) => {
        elements.forEach((element) => {

            const index= element.getBoundingClientRect()
            if(index.bottom < 0 || index.top > heightWindow) {
                element.classList.remove('start')
            } else if (index.top < heightWindow ) {
                element.classList.add('start')
            }
        })
    })
}

//  video
const video = $('video')
video.addEventListener('canplay', () => {
    setTimeout(() => {
        video.classList.add('start' ,'autoplay')
        video.play()
    }, 600)
})

//  animation goToTop
function handleGoToTop() {
    const goToTop= document.querySelector('.goToTop')
    goToTop.addEventListener('click', () => {
        document.body.scrollTop= 0
        document.documentElement.scrollTop = 0
    })
}

// animation when pageLoad
function pageLoad() {
    window.addEventListener('load', () => {
        handlePageLoad(dataPageLoadSlide)
        handlePageLoad(dataPageLoadAppear)
    })
}
 function handlePageLoad(data) {
    data.forEach((element, i) => {
        element.classList.add('start')
    })
 }
 handlePageLoad()

 


